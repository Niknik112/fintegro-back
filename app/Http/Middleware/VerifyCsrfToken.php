<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'http://fintegro-back/registration',
        'http://fintegro-back/login',
        'http://fintegro-back/site',
        'http://fintegro-back/profile',

    ];
//    protected $except = [
//        //
//        'http://fintegro-back.shool.front/registration',
//        'http://fintegro-back.shool.front/login',
//        'http://fintegro-back.shool.front/site',
//        'http://fintegro-back.shool.front/profile',
//
//    ];
}
