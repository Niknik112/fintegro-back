<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'CoreSiteController@show');

Route::get('/users', 'CoreSiteController@getUsers');

Route::get('/estn',['as'=>'input','uses'=>'CoreSiteController@showInput']);
//Route::match(['get','post'],'/estn',['as'=>'input','uses'=>'CoreSiteController@showInput']);

Route::post('/showinput',['as'=>'show','uses'=>'CoreSiteController@testInput']);

//Route::options('/login',['as'=>'login','uses'=>'CoreSiteController@login']);
Route::post('/login',['as'=>'login','uses'=>'CoreSiteController@login']);
//Route::match(['get','post'],'/login',['as'=>'login','uses'=>'CoreSiteController@login']);

Route::post('/registration',['as'=>'registration','uses'=>'CoreSiteController@registration']);

Route::post('/recovery',['as'=>'recovery','uses'=>'CoreSiteController@recovery']);

Route::get('/site',['as'=>'site','uses'=>'CoreSiteController@site']);


//Route::get(
//    return redirect()->route('login');
//    );



