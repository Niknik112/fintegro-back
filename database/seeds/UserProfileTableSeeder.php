<?php

use Illuminate\Database\Seeder;
use App\user_profile;

class UserProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        user_profile::create([

            'user_id' => "12",
            'firstname' => "nikolai",
            'lastname' => "zagr",
            'quote' => "werw",
            'photo' => "123erdf",
            'lived' => "er45",
            'from' => "34df",
            'went' => "345ffv"
        ]);
    }
}
